(function() {

	var aa = {}

	aa.Crawler = require('simplecrawler');
	
	var crawler = new aa.Crawler('devweb14.nanocosm.com');
	
	crawler.initialPath = '/'
	crawler.initialProtocol = "http";
	crawler.interval = 1000; 		
	crawler.maxConcurrency = 3;
	crawler.maxDepth = 2;			
	crawler.decodeResponses = true;
	crawler.stripQuerystring = false;
	
	var condition_noPROJECT = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\/project\//i);
	});
	var condition_noHTML = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.html/i);
	});
	var condition_noHTM = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.htm$/i);
	});	
	var condition_noJS = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.js$/i);
	});
	var condition_noCSS = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.css$/i);
	});
	var condition_noPDF = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.pdf$/i);
	});
	var condition_noTXT = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.txt$/i);
	});
	var condition_noZIP = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.zip$/i);
	});
	var condition_noPNG = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.png$/i);
	});
	var condition_noGIF = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.gif$/i);
	});
	var condition_noJPG = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.jpg$/i);
	});
	var condition_noJPEG = crawler.addFetchCondition(function(parsedURL) {
		return !parsedURL.path.match(/\.jpeg$/i);
	})


	crawler.on("queueerror", function(errorData, URLData) {
		console.log(">>> fetcherror:", errorData, URLData);
	});
	 
	crawler.on("fetch404", function(queueItem, response) {
		console.log("===");
		console.log("===");
		console.log(">>> fetch404:", queueItem);
		console.log("---");
		console.log(">>> fetch404:", response);
		console.log("=== ===");
	});
	
	crawler.on("fetchstart", function(queueItem, requestOptions) {

		console.log("===");
		console.log("===");
		
		///queueItem.path = queueItem.path + "?_escaped_liumei_crawler_fragment_=a"
		requestOptions.path = requestOptions.path + "?_escaped_liumei_crawler_fragment_=a"
		
		console.log(">>> FETCHSTART - PT1:", queueItem);
		console.log("---");
		console.log(">>> fetchstart - PT2:", requestOptions);
		console.log("=== ===");
	});

	crawler.on("fetcherror", function(queueItem, response ) {
		console.log(">>> fetcherror:", queueItem, response);
	});
	 
	crawler.on("fetchcomplete", function(queueItem, responseBody, responseObject ) {
		console.log("===");
		console.log("===")
		
		console.log(">>> FETCHCOMPLETE - PT1 - queueItem:", queueItem);
		console.log("---");
		console.log(">>> fetchcomplete - PT2 - responseBody:", responseBody.toString());
		
		console.log("=== ===");
	});

	crawler.on("fetchclienterror", function(queueItem, errorData) {
		console.log(">>> fetchclienterror:", queueItem, errorData);
	});
	

	crawler.start();
	
})()
