"use strict";

(function() {
	console.log("#######################################################");
	console.log("### ");
	console.log("### ");
	console.log("### ");
	console.log("### ");
	console.log("### ");
	console.log("### ");
	console.log("### ");


	var aa = {}

	aa.co = require("co");
	aa.exec = require('co-exec');
	aa.fs = require('co-fs');
	aa.request = require("co-request");

	aa.Crawler = require('simplecrawler');
	
	
	aa.clearTheInProgressFolder = function*(pwd) {	
		try {
			console.log(">>> clearTheInProgressFolder - started.");
			
			var hh = {};
			
			hh.voidd = yield aa.exec('rm -rf ' + pwd+'/Link_to_Rendered/SiteInProgress/')
			hh.voidd = yield aa.exec('mkdir ' + pwd+'/Link_to_Rendered/SiteInProgress/')
			hh.voidd = yield aa.exec('touch ' + pwd+'/Link_to_Rendered/SiteInProgress/0_empty')
			
			console.log(">>> clearTheInProgressFolder - done.");
		}
		catch(errorr) {
			console.log(">>> clearTheInProgressFolder error: ", errorr);
		}
	}
	
	aa.handleComplete = function*(pwd) {	
		try {
			console.log(">>> handleComplete - started.");
			
			var hh = {};
			
			hh.voidd = yield aa.exec('cp ' + pwd+'/Link_to_Rendered/SiteInProgress/_FSlash_.html ' + pwd+'/Link_to_Rendered/SiteInProgress/_FSlash_home.html')
			
			
			hh.voidd = yield aa.exec('rm -r ' + pwd+'/Link_to_Rendered/SiteCompleted/')
			hh.voidd = yield aa.exec('mv ' + pwd+'/Link_to_Rendered/SiteInProgress/ ' + pwd+'/Link_to_Rendered/SiteCompleted/')
			
			console.log(">>> handleComplete - done.");
		}
		catch(errorr) {
			console.log(">>> handleComplete error: ", errorr);
		}
	}
	

	
	aa.handleFetch = function*(pwd, requestPath, responseHTML) {	
		try {
			console.log(">>> handleFetch started - requestPath: ", requestPath);
			
			var hh = {};
			
			console.log(">>> handleFetch - A");
			
			hh.saveToFilename = requestPath.replace(/\//g, "_FSlash_")+'.html';
			
			console.log(">>> handleFetch - B");
			
			hh.saveTo = pwd+'/Link_to_Rendered/SiteInProgress/'+ hh.saveToFilename;
			
			console.log(">>> handleFetch - saving the responseHTML to : ", hh.saveTo);
			
			hh.fileContent = yield aa.fs.writeFile(hh.saveTo, responseHTML);
			
			console.log(">>> handleFetch done - responseHTML saved to: ", hh.saveTo);
		}
		catch(errorr) {
			console.log(">>> handleFetch error: ", errorr);
		}
	}
		
	
	aa.co(function* () {
		var vv = {};
		
		console.log(">>> Botcentral - start of co function.");
		vv.pwd = yield aa.exec('pwd')
		vv.pwd = vv.pwd.replace(/\r?\n|\r/, '');
		
		vv.voidd = yield aa.clearTheInProgressFolder(vv.pwd) 
		
		
		
		console.log(">>> Botcentral - pwd: ", vv.pwd);
		
		try {
			vv.schoolRegExpObj = new RegExp("(school/).+?/([0-9])", "i");
		}
		catch(errorr) {
			console.log(">>> Botcentral - schoolRegExpObj - error: ", errorr);
		}
		
		
				
		var crawler = new aa.Crawler('devweb14.nanocosm.com');
		
		crawler.initialPath = '/'
		crawler.initialProtocol = "http";
		crawler.interval = 1000; 		
		crawler.maxConcurrency = 3;
		crawler.maxDepth = 2;			
		crawler.decodeResponses = true;
		crawler.stripQuerystring = false;
		
		var condition_noJPEG = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\/home/i);
		})
		var condition_noPROJECT = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\/project\//i);
		});
		var condition_noHTML = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.html/i);
		});
		var condition_noHTM = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.htm$/i);
		});	
		var condition_noJS = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.js$/i);
		});
		var condition_noCSS = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.css$/i);
		});
		var condition_noPDF = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.pdf$/i);
		});
		var condition_noTXT = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.txt$/i);
		});
		var condition_noZIP = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.zip$/i);
		});
		var condition_noPNG = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.png$/i);
		});
		var condition_noGIF = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.gif$/i);
		});
		var condition_noJPG = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.jpg$/i);
		});
		var condition_noJPEG = crawler.addFetchCondition(function(parsedURL) {
			return !parsedURL.path.match(/\.jpeg$/i);
		})



		crawler.on("queueerror", function(errorData, URLData) {
			console.log(">>> fetcherror:", errorData, URLData);
		});
		 
		crawler.on("fetch404", function(queueItem, response) {
			console.log("===");
			console.log("===");
			console.log(">>> fetch404:", queueItem);
			console.log("---");
			console.log(">>> fetch404:", response);
			console.log("=== ===");
		});
		
		crawler.on("fetchstart", function(queueItem, requestOptions) {

			console.log("===");
			console.log("===");
			
			///queueItem.path = queueItem.path + "?_escaped_liumei_crawler_fragment_=a"
			requestOptions.myOriginalPath = requestOptions.path
			requestOptions.path = requestOptions.path.replace(vv.schoolRegExpObj, '$1$2')
			requestOptions.path = requestOptions.path + "?_escaped_liumei_crawler_fragment_=a"
			
			console.log(">>> FETCHSTART - PT1:", queueItem);
			console.log("---");
			console.log(">>> fetchstart - PT2:", requestOptions);
			console.log("=== ===");
		});

		crawler.on("fetcherror", function(queueItem, response ) {
			console.log(">>> fetcherror:", queueItem, response);
		});
		
		

				
				
		 
		crawler.on("fetchcomplete", function(queueItem, responseBody, responseObject ) {
			console.log("===");
			console.log("===")
			
			var vvv = {};
			
			queueItem.myRequestedPath = queueItem.path.replace(vv.schoolRegExpObj, '$1$2')
			vvv.requestedPath = queueItem.myRequestedPath
			vvv.responseHTML = responseBody.toString()
			
			
			console.log(">>> FETCHCOMPLETE - PT1 - queueItem:", queueItem);
			console.log("---");
			console.log(">>> fetchcomplete - PT2 - responseHTML length:", vvv.responseHTML.length);
			
			//{	Dump to file:
				aa.co(function* () {
					vvv.voidd = yield aa.handleFetch(vv.pwd, vvv.requestedPath, vvv.responseHTML) 
				}).catch(function (err) {
					console.err(err);
				});
			//}
			
			console.log("=== ===");
		});

		crawler.on("fetchclienterror", function(queueItem, errorData) {
			console.log(">>> fetchclienterror:", queueItem, errorData);
		});
	
		crawler.on("complete", function() {
			console.log("===");
			console.log("===")
			
			var vvv = {};
			console.log(">>> oncomplete started");
			
			//{	Switch InProgress to Completed:
				aa.co(function* () {
					vvv.voidd = yield aa.handleComplete(vv.pwd) 
				}).catch(function (err) {
					console.err(err);
				});
			//}
			
			console.log(">>> oncomplete done");
			console.log("=== ===");
		});	
		
 

		crawler.start();
		
	}).catch(function (err) {
		console.err(err);
	});
	
})()
